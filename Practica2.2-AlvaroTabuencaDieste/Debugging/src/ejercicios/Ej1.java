package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		for(int i = 0 ; i <= cadenaLeida.length(); i++){
			
		/* Como i est� igualada a 0, e intentas forzarla a que recorra un car�cter m�s 
		 * del que tiene la cadena porque en la condici�n indicas que i 
		 * tiene que ser menor o igual a la longitud de la cadena, da error.
		 
		 * Dos formas de solucionarlo: Haciendo en la condici�n que i sea menor 
			a la longitud de cadenaLeida, o que sea menor o igual a la longitud de 
			cadenaLeida -1.	
		 */
			
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
