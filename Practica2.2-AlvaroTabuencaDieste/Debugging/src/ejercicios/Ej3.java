package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		/* Da error a la hora de sacar la cadena final porque el char est� igualado a un n�mero, 
		 * no a un c�racter.
		 * La soluci�n ser�a igualar el char a un car�cter y no a un n�mero (' ')
		 */
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
