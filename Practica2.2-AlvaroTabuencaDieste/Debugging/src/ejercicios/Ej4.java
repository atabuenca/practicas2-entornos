package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			
			/* El programa da error porque, como en el bucle i est� inicializado a 0, 
			 * y la condici�n es que se entre al bucle cuando i sea mayor o IGUAL a 0, 
			 * intenta dividir el n�mero entero por 0, por lo que da error.
			 * La soluci�n ser�a corregir la condici�n del bucle para que i>0, sin el igual, 
			 * para que as� en el momento en que i sea 0 no entre al bucle 
			 * y no intente dividir por �l.
			*/
			
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
