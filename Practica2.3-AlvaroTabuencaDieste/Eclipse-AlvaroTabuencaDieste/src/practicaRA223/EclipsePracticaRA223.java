package practicaRA223;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JRadioButton;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;

// Pr�ctica 2.3 del RA2 realizada por �lvaro Tabuenca Dieste.

/**
 * 
 * @author �lvaro Tabuenca Dieste.
 * @since 25/01/2018
 * 
 */

public class EclipsePracticaRA223 extends JFrame {

	private JPanel contentPane;
	private JTextField txtSeleccionaElColor;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EclipsePracticaRA223 frame = new EclipsePracticaRA223();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EclipsePracticaRA223() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(EclipsePracticaRA223.class.getResource("/imagenes/Icono.jpg")));
		setTitle("Pr\u00E1ctica 2.3 - \u00C1lvaro Tabuenca Dieste");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 569, 386);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Captura de pantalla");
		mnArchivo.add(mntmNewMenuItem);
		
		JMenuItem mntmGuardarComposicin = new JMenuItem("Guardar composici\u00F3n");
		mnArchivo.add(mntmGuardarComposicin);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenu mnNewMenu = new JMenu("Combinaciones t\u00EDpicas");
		mnAyuda.add(mnNewMenu);
		
		JMenuItem mntmRojo = new JMenuItem("Rojo");
		mnNewMenu.add(mntmRojo);
		
		JMenuItem mntmAzul = new JMenuItem("Azul");
		mnNewMenu.add(mntmAzul);
		
		JMenuItem mntmVerde = new JMenuItem("Verde");
		mnNewMenu.add(mntmVerde);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(35, 60, 89, 23);
		contentPane.add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(151, 60, 89, 23);
		contentPane.add(btnCancelar);
		
		txtSeleccionaElColor = new JTextField();
		txtSeleccionaElColor.setEditable(false);
		txtSeleccionaElColor.setText("Selecciona el color que m\u00E1s te guste");
		txtSeleccionaElColor.setBounds(187, 11, 219, 20);
		contentPane.add(txtSeleccionaElColor);
		txtSeleccionaElColor.setColumns(10);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(35, 109, 469, 184);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.RED);
		tabbedPane.addTab("Rojo", null, panel, null);
		tabbedPane.setBackgroundAt(0, Color.RED);
		panel.setLayout(null);
		
		JRadioButton rdbtnMagenta = new JRadioButton("Magenta");
		buttonGroup.add(rdbtnMagenta);
		rdbtnMagenta.setBounds(6, 18, 109, 23);
		panel.add(rdbtnMagenta);
		
		JRadioButton rdbtnRojoSangre = new JRadioButton("Rojo sangre");
		buttonGroup.add(rdbtnRojoSangre);
		rdbtnRojoSangre.setBounds(6, 57, 109, 23);
		panel.add(rdbtnRojoSangre);
		
		JRadioButton rdbtnCobrizo = new JRadioButton("Cobrizo");
		buttonGroup.add(rdbtnCobrizo);
		rdbtnCobrizo.setBounds(6, 97, 109, 23);
		panel.add(rdbtnCobrizo);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(0, -10, 10, 1));
		spinner.setBounds(273, 58, 41, 20);
		panel.add(spinner);
		
		JLabel lblAumentarODisminuir = new JLabel("Aumentar o disminuir tonalidad");
		lblAumentarODisminuir.setBounds(206, 22, 194, 14);
		panel.add(lblAumentarODisminuir);
		
		JSlider slider = new JSlider();
		slider.setValue(0);
		slider.setMajorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMaximum(10);
		slider.setMinimum(-10);
		slider.setBounds(151, 97, 292, 48);
		panel.add(slider);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.BLUE);
		tabbedPane.addTab("Azul", null, panel_2, null);
		tabbedPane.setBackgroundAt(1, Color.BLUE);
		panel_2.setLayout(null);
		
		JLabel label = new JLabel("Aumentar o disminuir tonalidad");
		label.setBounds(214, 25, 209, 14);
		panel_2.add(label);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(0, -10, 10, 1));
		spinner_1.setBounds(277, 55, 41, 20);
		panel_2.add(spinner_1);
		
		JSlider slider_1 = new JSlider();
		slider_1.setValue(0);
		slider_1.setPaintTicks(true);
		slider_1.setPaintLabels(true);
		slider_1.setMinimum(-10);
		slider_1.setMaximum(10);
		slider_1.setMajorTickSpacing(1);
		slider_1.setBounds(151, 97, 292, 48);
		panel_2.add(slider_1);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Azul");
		buttonGroup_1.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(6, 21, 109, 23);
		panel_2.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnCyan = new JRadioButton("Cyan");
		buttonGroup_1.add(rdbtnCyan);
		rdbtnCyan.setBounds(6, 64, 109, 23);
		panel_2.add(rdbtnCyan);
		
		JRadioButton rdbtnAzulCielo = new JRadioButton("Azul cielo");
		buttonGroup_1.add(rdbtnAzulCielo);
		rdbtnAzulCielo.setBounds(6, 106, 109, 23);
		panel_2.add(rdbtnAzulCielo);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.GREEN);
		tabbedPane.addTab("Verde", null, panel_4, null);
		tabbedPane.setBackgroundAt(2, Color.GREEN);
		panel_4.setLayout(null);
		
		JRadioButton rdbtnVerdeClaro = new JRadioButton("Verde claro");
		buttonGroup_2.add(rdbtnVerdeClaro);
		rdbtnVerdeClaro.setBounds(6, 23, 109, 23);
		panel_4.add(rdbtnVerdeClaro);
		
		JRadioButton rdbtnVerdeOscuro = new JRadioButton("Verde oscuro");
		buttonGroup_2.add(rdbtnVerdeOscuro);
		rdbtnVerdeOscuro.setBounds(6, 65, 109, 23);
		panel_4.add(rdbtnVerdeOscuro);
		
		JRadioButton rdbtnEsmeralda = new JRadioButton("Esmeralda");
		buttonGroup_2.add(rdbtnEsmeralda);
		rdbtnEsmeralda.setBounds(6, 113, 109, 23);
		panel_4.add(rdbtnEsmeralda);
		
		JLabel label_1 = new JLabel("Aumentar o disminuir tonalidad");
		label_1.setBounds(206, 27, 209, 14);
		panel_4.add(label_1);
		
		JSlider slider_2 = new JSlider();
		slider_2.setValue(0);
		slider_2.setPaintTicks(true);
		slider_2.setPaintLabels(true);
		slider_2.setMinimum(-10);
		slider_2.setMaximum(10);
		slider_2.setMajorTickSpacing(1);
		slider_2.setBounds(151, 97, 292, 48);
		panel_4.add(slider_2);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setModel(new SpinnerNumberModel(0, -10, 10, 1));
		spinner_2.setBounds(277, 54, 41, 20);
		panel_4.add(spinner_2);
		
		JCheckBox chckbxcontactarPorCorreo = new JCheckBox("\u00BFContactar por correo?");
		chckbxcontactarPorCorreo.setBounds(257, 60, 171, 23);
		contentPane.add(chckbxcontactarPorCorreo);
		
		JButton btnContactar = new JButton("Contactar");
		btnContactar.setBounds(434, 60, 110, 23);
		contentPane.add(btnContactar);
	}
}
