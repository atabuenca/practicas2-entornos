package practica24Java;
import java.util.Scanner;

public class Practica24Java {
	
	static void parOimpar(int num)
	{
		if(num%2==0)
		{
			System.out.println("El n�mero es par.");
		}
		
		else
		{
			System.out.println("El n�mero es impar.");
		}
	}
	
	static void primo(int numeroPasado)
	{
		int contadorDivisores=0;
		
		for(int i=1;i<=numeroPasado;i++)
		{
			if(numeroPasado%i==0)
			{
				contadorDivisores++;
			}
		}
		
		if(contadorDivisores==2)
		{
			System.out.println("El n�mero es primo.");
		}
		
		else
		{
			System.out.println("El n�mero no es primo.");
		}
	}
	
	static void positivoOnegativo(int numeroEvaluar)
	{
		if(numeroEvaluar>=0)
		{
			System.out.println("El n�mero es positivo.");
		}
		
		else
		{
			System.out.println("El n�mero es negativo.");
		}
	}
	
	static int sumaNumeros(int num1, int num2)
	{
		int resultado;
		resultado=num1+num2;
		return resultado;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input=new Scanner(System.in);
		int opcion;
		int numero;
		int numero2;
		int suma;
		
		System.out.println("*************MEN�****************");
		System.out.println("1. N�mero par o impar.");
		System.out.println("2. N�mero primo o no.");
		System.out.println("3. N�mero positivo o negativo.");
		System.out.println("4. Suma de dos n�meros.");
		System.out.println("Elige la opci�n que deseas:");
		opcion=input.nextInt();
		
		switch(opcion)
		{
			case 1:
				
				System.out.println("Introduce un n�mero:");
				numero=input.nextInt();
				
				parOimpar(numero);
			
			break;
			
			case 2:
				System.out.println("Introduce un n�mero:");
				numero=input.nextInt();
				
				primo(numero);
				
			break;
			
			case 3:
				
				System.out.println("Introduce un n�mero:");
				numero=input.nextInt();
				
				positivoOnegativo(numero);
				
			break;
			
			case 4:
				
				System.out.println("Introduce el primer n�mero:");
				numero=input.nextInt();
				
				System.out.println("Introduce el segundo n�mero:");
				numero2=input.nextInt();
				
				suma=sumaNumeros(numero, numero2);
				
				System.out.println("La suma de "+numero+" y "+numero2+" es: "+suma);
				
			break;
			
			default:
				
				System.out.println("Opci�n incorrecta. Fin del programa.");
		}
		
		input.close();
	}

}
