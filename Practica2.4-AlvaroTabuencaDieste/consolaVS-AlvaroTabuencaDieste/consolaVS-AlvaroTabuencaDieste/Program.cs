﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_AlvaroTabuencaDieste
{
    class Program
    {
        static void parOimpar(int num)
        {
            if (num % 2 == 0)
            {
                Console.WriteLine("El número es par.");
            }

            else
            {
                Console.WriteLine("El número es impar.");
            }
        }

        static void primo(int numeroPasado)
        {
            int contadorDivisores = 0;

            for (int i = 1; i <= numeroPasado; i++)
            {
                if (numeroPasado % i == 0)
                {
                    contadorDivisores++;
                }
            }

            if (contadorDivisores == 2)
            {
                Console.WriteLine("El número es primo.");
            }

            else
            {
                Console.WriteLine("El número no es primo.");
            }
        }

        static void positivoOnegativo(int numeroEvaluar)
        {
            if (numeroEvaluar >= 0)
            {
                Console.WriteLine("El número es positivo.");
            }

            else
            {
                Console.WriteLine("El número es negativo.");
            }
        }

        static int sumaNumeros(int num1, int num2)
        {
            int resultado;
            resultado = num1 + num2;
            return resultado;
        }

        static void Main(string[] args)
        {
            int opcion;
            String eleccion;
            int numero;
            String auxiliar;
            int numero2;
            String auxiliar2;
            int suma;

            Console.WriteLine("*************MENÚ****************");
            Console.WriteLine("1. Número par o impar.");
            Console.WriteLine("2. Número primo o no.");
            Console.WriteLine("3. Número positivo o negativo.");
            Console.WriteLine("4. Suma de dos números.");
            Console.WriteLine("Elige la opción que deseas:");
            eleccion = Console.ReadLine();
            opcion = int.Parse(eleccion);

            switch (opcion)
            {
                case 1:

                    Console.WriteLine("Introduce un número:");
                    numero = Console.Read();

                    parOimpar(numero);

                    break;

                case 2:
                    Console.WriteLine("Introduce un número:");
                    auxiliar = Console.ReadLine();
                    numero = int.Parse(auxiliar);

                    primo(numero);

                    break;

                case 3:

                    Console.WriteLine("Introduce un número:");
                    auxiliar = Console.ReadLine();
                    numero = int.Parse(auxiliar);

                    positivoOnegativo(numero);

                    break;

                case 4:

                    Console.WriteLine("Introduce el primer número:");
                    auxiliar = Console.ReadLine();
                    numero = int.Parse(auxiliar);

                    Console.WriteLine("Introduce el segundo número:");
                    auxiliar2 = Console.ReadLine();
                    numero2 = int.Parse(auxiliar2);

                    suma = sumaNumeros(numero, numero2);

                    Console.WriteLine("La suma de " + numero + " y " + numero2 + " es: " + suma);

                    break;

                default:

                    Console.WriteLine("Opción incorrecta. Fin del programa.");

                break;
            }

            Console.WriteLine("Pulse una tecla para continuar...");
            Console.ReadKey();

        }
    }
}
