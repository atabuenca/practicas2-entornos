package practica25Java;

public class Practica25Java {

	public static boolean numeroPrimo(int numeroEvaluar)
	{
		int contadorDivisores=0;
		
		for(int i=1;i<=numeroEvaluar;i++)
		{
			if(numeroEvaluar%i==0)
			{
				contadorDivisores++;
			}
		}
		
		if(contadorDivisores==2)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	public static boolean numeroPerfecto(int numeroAnalizar)
	{	
		int resultado=0;
		
		for(int i=1;i<numeroAnalizar;i++)
		{
			if(numeroAnalizar%i==0)
			{
				resultado=resultado+i;
			}
		}
		
		if(resultado==numeroAnalizar)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	public static int potencia(int base, int exponente)
	{
		int resultado=base;
		
		for(int i=1;i<exponente;i++)
		{
			resultado=resultado*base;
		}
		
		return resultado;
	}
	
	public static boolean numeroPar(int numeroComprobar)
	{
		if(numeroComprobar%2==0)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	public static boolean numeroImpar(int numeroPasado)
	{
		if(numeroPasado%2!=0)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		
	}

}
