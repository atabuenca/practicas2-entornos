﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_AlvaroTabuencaDieste
{
    public class Class1
    {
        public static bool numeroPositivo(int numeroPasado)
        {
            if (numeroPasado >= 0)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public static bool numeroNegativo(int numeroAnalizar)
        {
            if (numeroAnalizar <= 0)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public static void numeroCapicua(int numeroEvaluar)
        {
            int numeroInvertido = 0;
            int resto = 0;
            int faltante = numeroEvaluar;

            while (faltante > 0)
            {
                resto = faltante % 10;
                numeroInvertido = numeroInvertido * 10 + resto;
                faltante = faltante / 10;
            }

            if (numeroInvertido == numeroEvaluar)
            {
                Console.WriteLine("El número es capicúa.");
            }

            else
            {
                Console.WriteLine("El número no es capicúa.");
            }

        }

        public static void numerosAmigos(int num1, int num2)
        {
            int suma1 = 0;
            int suma2 = 0;

            for (int i = 1; i < num1; i++)
            {
                if (num1 % i == 0)
                {
                    suma1 = suma1 + i;
                }
            }

            if (suma1 == num2)
            {
                for (int j = 1; j < num2; j++)
                {
                    if (num2 % j == 0)
                    {
                        suma2 = suma2 + j;
                    }
                }

                if (suma2 == num1)
                {
                    Console.WriteLine("Los números son amigos.");
                }

                else
                {
                    Console.WriteLine("Los números no son amigos.");
                }
            }

            else
            {
                Console.WriteLine("Los números no son amigos.");
            }
        }

        public static bool numeroCompuesto(int numeroComprobar)
        {
            int contadorDivisores = 0;

            for (int i = 1; i <= numeroComprobar; i++)
            {
                if (numeroComprobar % i == 0)
                {
                    contadorDivisores++;
                }
            }

            if (contadorDivisores == 2 || numeroComprobar == 1)
            {
                return false;
            }

            else
            {
                return true;
            }
        }
    }
}
