package comprobacionLibreriaJava;
import practica25Java.*;

public class ComprobacionLibreriaJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numero1=8;
		int numero2=3;
		boolean evaluacion;
		int resultado;
		
		evaluacion=Practica25Java.numeroPerfecto(numero1);
		
		if(!evaluacion)
		{
			System.out.println("El n�mero no es perfecto.");
		}
		
		else
		{
			System.out.println("El n�mero es perfecto.");
		}
		
		evaluacion=Practica25Java.numeroImpar(numero1);
		
		if(!evaluacion)
		{
			System.out.println("El n�mero no es impar.");
		}
		
		else
		{
			System.out.println("El n�mero es impar.");
		}
		
		evaluacion=Practica25Java.numeroPar(numero1);
		
		if(!evaluacion)
		{
			System.out.println("El n�mero no es par.");
		}
		
		else
		{
			System.out.println("El n�mero es par.");
		}
		
		evaluacion=Practica25Java.numeroPrimo(numero1);
		
		if(!evaluacion)
		{
			System.out.println("El n�mero no es primo.");
		}
		
		else
		{
			System.out.println("El n�mero es primo.");
		}
		
		resultado=Practica25Java.potencia(numero1, numero2);
		
		System.out.println("El resultado de "+numero1+" elevado a "+numero2+" es: "+resultado);
	}

}
