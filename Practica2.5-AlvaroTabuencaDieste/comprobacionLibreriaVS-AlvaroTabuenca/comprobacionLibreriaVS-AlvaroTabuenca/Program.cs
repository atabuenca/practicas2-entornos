﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_AlvaroTabuencaDieste;

namespace comprobacionLibreriaVS_AlvaroTabuenca
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero1 = 220;
            int numero2 = 284;
            bool evaluacion;

            Class1.numerosAmigos(numero1, numero2);

            Class1.numeroCapicua(numero1);

            evaluacion = Class1.numeroCompuesto(numero1);

            if(!evaluacion)
            {
                Console.WriteLine("El número no es compuesto.");
            }

            else
            {
                Console.WriteLine("El número es compuesto.");
            }

            evaluacion = Class1.numeroNegativo(numero1);

            if(!evaluacion)
            {
                Console.WriteLine("El número no es negativo.");
            }

            else
            {
                Console.WriteLine("El número es negativo.");
            }

            evaluacion = Class1.numeroPositivo(numero1);

            if(!evaluacion)
            {
                Console.WriteLine("El número no es positivo.");
            }

            else
            {
                Console.WriteLine("El número es positivo.");
            }

            Console.WriteLine("Presiona una tecla para continuar...");
            Console.ReadKey();
        }
    }
}
